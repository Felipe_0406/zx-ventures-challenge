CREATE TABLE partners (
   id INT4 NOT NULL,
   area_id VARCHAR(50) NOT NULL,
   trading_name VARCHAR(120),
   owner_name VARCHAR(120),
   document VARCHAR(240),
   latitude DOUBLE,
   longitude DOUBLE,
   coverage_area_type VARCHAR(320),
   coverage_area VARCHAR(120),
   address_type VARCHAR(120),
   address VARCHAR(120),
   PRIMARY KEY(id)
);