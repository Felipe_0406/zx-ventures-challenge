package com.zedelivery.codechallenge.model.io;

import java.util.List;

public class GeoJSonArea {

    private String type;
    private List<List<Float[][]>> coordinates;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<List<Float[][]>> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(List<List<Float[][]>> coordinates) {
        this.coordinates = coordinates;
    }
}
