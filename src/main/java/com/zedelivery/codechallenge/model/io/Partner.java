package com.zedelivery.codechallenge.model.io;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Partner
 */
public class Partner {

    @NotNull
    private Long id;

    @NotBlank
    private String tradingName;

    @NotBlank
    private String ownerName;

    @NotBlank
    private String document;

    @NotNull
    private GeoJSonArea coverageArea;

    @NotNull
    private GeoJSonPoint address;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTradingName() {
        return tradingName;
    }

    public void setTradingName(String tradingName) {
        this.tradingName = tradingName;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public GeoJSonArea getCoverageArea() {
        return coverageArea;
    }

    public void setCoverageArea(GeoJSonArea coverageArea) {
        this.coverageArea = coverageArea;
    }

    public GeoJSonPoint getAddress() {
        return address;
    }

    public void setAddress(GeoJSonPoint address) {
        this.address = address;
    }
}
