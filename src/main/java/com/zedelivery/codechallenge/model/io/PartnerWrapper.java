package com.zedelivery.codechallenge.model.io;

import java.util.List;

/**
 * Partner Wrapper
 */
public class PartnerWrapper {

    private List<Partner> pdvs;

    public List<Partner> getPdvs() {
        return pdvs;
    }

    public void setPdvs(List<Partner> pdvs) {
        this.pdvs = pdvs;
    }
}
