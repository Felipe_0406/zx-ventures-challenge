package com.zedelivery.codechallenge.controller;

import com.zedelivery.codechallenge.model.io.Partner;
import com.zedelivery.codechallenge.service.PartnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Partner Controller
 */
@RestController
@CrossOrigin
@RequestMapping(path = "/pdvs/partner")
public class PartnerController {

    @Autowired
    private PartnerService partnerService;

    /**
     * Generate Partner
     * @param partner Object Partner
     * @return Status
     */
    @PostMapping
    public ResponseEntity addPartner(@Valid @RequestBody Partner partner){
        return partnerService.createPartner(partner);
    }

    /**
     * Get Partner by ID
     * @param partnerId Partner Id
     * @return Partner
     */
    @GetMapping("/{partnerId}")
    public ResponseEntity getPartner(@PathVariable Long partnerId){
        return partnerService.getPartnerById(partnerId);
    }

    /**
     * Get Partner by lat and lng
     * @param lat Latitude
     * @param lng longitude
     * @return Partner
     */
    @GetMapping("/lng/{lng}/lat/{lat}")
    public ResponseEntity getPartner(@PathVariable Float lng, @PathVariable Float lat){
        return partnerService.findPartnerByLngLat(lng, lat);
    }
}