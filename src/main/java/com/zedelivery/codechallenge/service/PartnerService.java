package com.zedelivery.codechallenge.service;

import com.zedelivery.codechallenge.model.io.Partner;
import org.springframework.http.ResponseEntity;

/**
 * Partner Service
 */
public interface PartnerService {

    ResponseEntity createPartner(Partner partner);

    ResponseEntity getPartnerById(Long id);

    ResponseEntity findPartnerByLngLat(Float lng, Float lat);
}
