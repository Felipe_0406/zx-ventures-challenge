package com.zedelivery.codechallenge.service.impl;

import com.zedelivery.codechallenge.model.Point;
import com.zedelivery.codechallenge.model.database.PartnerEntity;
import com.zedelivery.codechallenge.model.io.GeoJSonArea;
import com.zedelivery.codechallenge.model.io.GeoJSonPoint;
import com.zedelivery.codechallenge.model.io.Partner;
import com.zedelivery.codechallenge.model.io.PartnerWrapper;
import com.zedelivery.codechallenge.service.GeoPartnerService;
import com.zedelivery.codechallenge.util.PartnerUtil;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Geo functions for partner
 */
@Service
public class GeoPartnerServiceImpl implements GeoPartnerService {

    /**
     * Format entity, including area
     *
     * @param partner Partner
     * @return Partner Entity
     */
    @Override
    public PartnerEntity formatEntity(Partner partner) {
        PartnerEntity model = new PartnerEntity();

        String codeAddress = PartnerUtil.codePoint(partner.getAddress().getCoordinates());
        model.setAddress(codeAddress);
        model.setAddressType(partner.getAddress().getType());

        String codeArea = PartnerUtil.codeArea(partner.getCoverageArea().getCoordinates());
        model.setCoverageArea(codeArea);
        model.setCoverageAreaType(partner.getCoverageArea().getType());

        model.setDocument(partner.getDocument());
        model.setId(partner.getId());
        model.setOwnerName(partner.getOwnerName());
        model.setTradingName(partner.getTradingName());
        Float lng = PartnerUtil.getLongitude(partner);
        Float lat = PartnerUtil.getLatitude(partner);
        model.setAreaId(PartnerUtil.getAreaId(lng, lat));
        model.setLatitude(lat);
        model.setLongitude(lng);
        return model;
    }

    /**
     * Format partner from entity
     *
     * @return
     */
    public Partner formatPartner(PartnerEntity partnerEntity) {

        GeoJSonPoint address = new GeoJSonPoint();
        Float[] AddressPoint = PartnerUtil.decodePoint(partnerEntity.getAddress());
        address.setCoordinates(AddressPoint);
        address.setType(partnerEntity.getAddressType());

        GeoJSonArea coverageArea = new GeoJSonArea();
        List<List<Float[][]>> area = PartnerUtil.decodeArea(partnerEntity.getCoverageArea());
        coverageArea.setCoordinates(area);
        coverageArea.setType(partnerEntity.getCoverageAreaType());

        Partner model = new Partner();
        model.setAddress(address);
        model.setCoverageArea(coverageArea);
        model.setDocument(partnerEntity.getDocument());
        model.setId(partnerEntity.getId());
        model.setOwnerName(partnerEntity.getOwnerName());
        model.setTradingName(partnerEntity.getTradingName());
        return model;
    }

    /**
     * Identify Partners from zone
     *
     * @param partnerZone List of Partners near to the point
     * @param lat         latitude
     * @param lng         longitude
     * @return Partners in the zone
     */
    @Override
    public PartnerWrapper getPartnersFromZone(List<Partner> partnerZone, Float lng, Float lat) {
        Point p = new Point();
        p.X = lng;
        p.Y = lat;

        List<Partner> partnersOnZone = new ArrayList<>();
        for (Partner partner : partnerZone) {
            for(Point[] area : getArea(partner)){
                if (this.isPointInArea(p, area)) {
                    partnersOnZone.add(partner);
                }
            }
        }
        //Create Wrapper
        PartnerWrapper partners = new PartnerWrapper();
        partners.setPdvs(partnersOnZone);

        return partners;
    }


    /**
     * Winding number algorithm
     * @param p Point
     * @param area Area
     * @return
     */
    public Boolean isPointInArea( Point p, Point[] area ) {
        double minX = area[0].X;
        double maxX = area[0].X;
        double minY = area[0].Y;
        double maxY = area[0].Y;

        for(int i=1 ;i < area.length; i++){
            Point q = area[ i ];
            minX = Math.min(q.X, minX);
            maxX = Math.max(q.X, maxX);
            minY = Math.min(q.Y, minY);
            maxY = Math.max(q.Y, maxY);
        }

        if(p.X < minX || p.X > maxX || p.Y < minY || p.Y > maxY){
            return false;
        }

        Boolean inside = false;
        for(int i = 0, j = area.length - 1 ; i < area.length ; j = i++){
            if( ( area[ i ].Y > p.Y ) != ( area[ j ].Y > p.Y ) &&
                    p.X < ( area[ j ].X - area[ i ].X ) * ( p.Y - area[ i ].Y ) / ( area[ j ].Y - area[ i ].Y ) + area[ i ].X ) {
                inside = !inside;
            }
        }
        return inside;
    }

    /**
     * Convert Area to Points
     * @param partner partner
     * @return Array
     */
    private List<Point[]> getArea(Partner partner){
        List<List<Float[][]>> listArea = partner.getCoverageArea().getCoordinates();
        List<Point[]> areas = new ArrayList<>();
        for(List<Float[][]> areaInside : listArea){
            Float[][] aux = areaInside.get(0);
            Point[] points = new Point[aux.length];
            for(int i=0; i<aux.length; i++){
                Point p = new Point();
                p.X = aux[i][0];
                p.Y = aux[i][1];
                points[i] = p;
            }
            areas.add(points);
        }
        return areas;
    }
}
