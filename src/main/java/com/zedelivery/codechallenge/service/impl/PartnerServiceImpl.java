package com.zedelivery.codechallenge.service.impl;

import com.zedelivery.codechallenge.model.database.PartnerEntity;
import com.zedelivery.codechallenge.model.io.Partner;
import com.zedelivery.codechallenge.model.io.PartnerWrapper;
import com.zedelivery.codechallenge.repository.CommonDbRepository;
import com.zedelivery.codechallenge.repository.InsertEntityRepository;
import com.zedelivery.codechallenge.service.GeoPartnerService;
import com.zedelivery.codechallenge.service.PartnerService;
import com.zedelivery.codechallenge.util.PartnerUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Partner Service Implement
 */
@Service
public class PartnerServiceImpl implements PartnerService {

    @Autowired
    private GeoPartnerService geoPartnerService;

    @Autowired
    private CommonDbRepository dataBaseRepository;

    @Autowired
    private InsertEntityRepository insertEntityRepository;

    /**
     * Create Partner
     * @param partner Partner Object
     * @return Response
     */
    @Override
    public ResponseEntity createPartner(Partner partner){
        try{
            //Create DB Model
            PartnerEntity entity = geoPartnerService.formatEntity(partner);

            //Get Data
            Long id = insertEntityRepository.insertWithQuery(entity);

            //Format Response
            return PartnerUtil.formatCreationResponse(id);

        } catch (DataIntegrityViolationException e) {
            return PartnerUtil.formatErrorResponse("The partner is already created", HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return PartnerUtil.formatErrorResponse(e, HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Get Partner by Id
     * @param id Partner Id
     * @return Response
     */
    @Override
    public ResponseEntity getPartnerById(Long id){
        Optional<PartnerEntity> optional = dataBaseRepository.findById(id);
        if(optional.isPresent()){
            return ResponseEntity.status(HttpStatus.OK)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(geoPartnerService.formatPartner(optional.get()));
        }
        return PartnerUtil.formatErrorResponse("Partner Not Found", HttpStatus.NOT_FOUND);
    }

    /**
     * Find Partner By Lat and lng
     * @param lng latitude
     * @param lat longitude
     * @return Response
     */
    @Override
    public ResponseEntity findPartnerByLngLat(Float lng, Float lat){
        //Get Area Id
        String areaId = PartnerUtil.getAreaId(lng, lat);

        //Get nearest partner
        List<PartnerEntity> partnersEntities = dataBaseRepository.findByAreaId(areaId);

        //Convert to Model
        List<Partner> partnerZone = new ArrayList<>();
        for(PartnerEntity entity: partnersEntities){
            partnerZone.add(geoPartnerService.formatPartner(entity));
        }

        //Get partners in the zone
        PartnerWrapper partnerModel = geoPartnerService.getPartnersFromZone(partnerZone, lng, lat );

        //Partner not found
        if(partnerModel == null){
            return PartnerUtil.formatErrorResponse("Parent Not Found", HttpStatus.NOT_FOUND);
        }

        //Return Response
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(partnerModel);
    }
}
