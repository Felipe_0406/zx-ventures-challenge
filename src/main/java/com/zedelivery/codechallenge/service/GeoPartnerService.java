package com.zedelivery.codechallenge.service;

import com.zedelivery.codechallenge.model.database.PartnerEntity;
import com.zedelivery.codechallenge.model.io.Partner;
import com.zedelivery.codechallenge.model.io.PartnerWrapper;

import java.util.List;

/**
 * Geo functions for partner
 */
public interface GeoPartnerService {

    PartnerEntity formatEntity(Partner partner);

    Partner formatPartner(PartnerEntity partnerEntity);

    PartnerWrapper getPartnersFromZone(List<Partner> partnerZone, Float lng, Float lat);
}
