package com.zedelivery.codechallenge.repository;

import com.zedelivery.codechallenge.model.database.PartnerEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Repository
public class InsertEntityRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public Long insertWithQuery(PartnerEntity partner) {

        String querySql = "INSERT INTO partners (id,area_id,trading_name,owner_name,document," +
                          "latitude,longitude,coverage_area_type,coverage_area,address_type,address ) " +
                          "VALUES (?,?,?,?,?,?,?,?,?,?,?)";
        entityManager.createNativeQuery(querySql)
                .setParameter(1, partner.getId())
                .setParameter(2, partner.getAreaId())
                .setParameter(3, partner.getTradingName())
                .setParameter(4, partner.getOwnerName())
                .setParameter(5, partner.getDocument())
                .setParameter(6, partner.getLatitude())
                .setParameter(7, partner.getLongitude())
                .setParameter(8, partner.getCoverageAreaType())
                .setParameter(9, partner.getCoverageArea())
                .setParameter(10, partner.getAddressType())
                .setParameter(11, partner.getAddress())
                .executeUpdate();

        return partner.getId();
    }
}
