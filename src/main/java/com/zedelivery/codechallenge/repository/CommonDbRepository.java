package com.zedelivery.codechallenge.repository;

import com.zedelivery.codechallenge.model.database.PartnerEntity;
import com.zedelivery.codechallenge.model.io.Partner;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CommonDbRepository extends JpaRepository<PartnerEntity, Long> {

    List<PartnerEntity> findByAreaId(String areaId);

    Optional<PartnerEntity> findById(Long id);
}
