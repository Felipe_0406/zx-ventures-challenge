package com.zedelivery.codechallenge.util;

import com.zedelivery.codechallenge.model.io.Partner;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public final class PartnerUtil {

    /**
     * Format Error Response
     * @param e Exception
     * @return Response
     */
    public static ResponseEntity formatErrorResponse(Exception e, HttpStatus status){
        Map<String, String> obj = new HashMap<>();
        obj.put("ErrorMessage", e.getMessage());
        return ResponseEntity.status(status)
                .contentType(MediaType.APPLICATION_JSON)
                .body(obj);
    }

    /**
     * Format Error Response
     * @param message message
     * @return Response
     */
    public static ResponseEntity formatErrorResponse(String message, HttpStatus status){
        Map<String, String> obj = new HashMap<>();
        obj.put("ErrorMessage", message);
        return ResponseEntity.status(status)
                .contentType(MediaType.APPLICATION_JSON)
                .body(obj);
    }

    /**
     * Format Creation Response
     * @param id Partner Id
     * @return Response
     */
    public static ResponseEntity formatCreationResponse(Long id){
        Map<String, String> obj = new HashMap<>();
        obj.put("Id", id.toString());
        return ResponseEntity.status(HttpStatus.CREATED)
                .contentType(MediaType.APPLICATION_JSON)
                .body(obj);
    }

    /**
     * Get Latitude (Y)
     * @param partner Partner
     * @return Latitude
     */
    public static Float getLatitude(Partner partner){
        return partner.getAddress().getCoordinates()[1];
    }

    /**
     * Get Longitude (Y)
     * @param partner Partner
     * @return Longitude
     */
    public static Float getLongitude(Partner partner){
        return partner.getAddress().getCoordinates()[0];
    }

    /**
     * Get Area Id
     * @param lng longitude
     * @param lat latitude
     * @return AreaId
     */
    public static String getAreaId(Float lng, Float lat){
        String result = (lng >= 0) ? "1" : "0" ;
        result += String.format("%06d", Math.abs((int)(lng * 1000f)));
        result += (lat >= 0) ? "1" : "0" ;
        result += String.format("%07d", Math.abs((int)(lat * 100000f)));
        return result;
    }

    /**
     * Code Array
     * @param point point
     * @return String
     */
    public static String codePoint(Float[] point){
        return point[0].toString() + ";" + point[1].toString();
    }

    /**
     * Code Array
     * @param areaListList area
     * @return String
     */
    public static String codeArea(List<List<Float[][]>> areaListList){
        String result = "";
        for(int i = 0; i<areaListList.size(); i++){
            result += codeMapArea(areaListList.get(i).get(0));
            if(i != areaListList.size()-1){
                result += "#";
            }
        }
        return result;
    }

    private static String codeMapArea(Float[][] mapArea){
        String result = "";
        for(int i = 0; i<mapArea.length; i++){
            result += mapArea[i][0].toString() + ":" + mapArea[i][1].toString();
            if(i != mapArea.length-1){
                result += ";";
            }
        }
        return result;
    }

    /**
     * Decode String
     * @param point point
     * @return array
     */
    public static Float[] decodePoint(String point){
        Float[] result = new Float[2];
        result[0] = Float.parseFloat(point.split(";")[0]);
        result[1] = Float.parseFloat(point.split(";")[1]);
        return result;
    }

    /**
     * Decode String
     * @param area area
     * @return array
     */
    public static List<List<Float[][]>> decodeArea(String area){
        List<List<Float[][]>> areaListList = new ArrayList<>();
        String[] areaListSplit = area.split("#");

        for(int j = 0; j < areaListSplit.length; j++){
            String[] areaSplit = areaListSplit[j].split(";");
            Float[][] areaDecode = new Float[areaSplit.length][2];
            for(int i = 0; i<areaSplit.length; i++){
                areaDecode[i][0] = Float.parseFloat(areaSplit[i].split(":")[0]);
                areaDecode[i][1] = Float.parseFloat(areaSplit[i].split(":")[1]);
            }
            List<Float[][]> areaList = new ArrayList<>();
            areaList.add(areaDecode);
            areaListList.add(areaList);
        }
        return areaListList;
    }
}
