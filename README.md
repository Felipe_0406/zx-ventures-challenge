# ZX-Ventures Challenge: Backend Challenge
### Author: Felipe Gonzalez Foncea
### Date: 12/06/19

## Features
The project uses an application spring boot with tomcat. The spring boot implements a H2 to persist data (this should be changed in a real enterprise environment).

## Requirements 
The application requires some tools for work. These should be integrated to the console of the operating system that is going to run the application.

- Java 8
- Maven

## How To Run
For execute code run the maven instruction
```sh
$ mvn spring-boot:run
```

## Endpoints

#### POST: http://localhost:8080/pdvs/partner
Create partner.

| Header | type |
| ------ | ------ |
| Content | application/json |

```
{
    "id": 1, 
    "tradingName": "Adega da Cerveja - Pinheiros",
    "ownerName": "Zé da Silva",
    "document": "1432132123891/0001", 
    "coverageArea": { 
        "type": "MultiPolygon", 
        "coordinates": [
            [[[30, 20], [45, 40], [10, 40], [30, 20]]], 
            [[[15, 5], [40, 10], [10, 20], [5, 10], [15, 5]]]
        ]
    }, 
    "address": { 
        "type": "Point",
        "coordinates": [-46.57421, -21.785741]
    } 
}
```


#### GET: http://localhost:8080/pdvs/partner/{ID}
Get partners by Id.

| Parameters | Value |
| ------ | ------ |
| ID | Partner Id |

#### GET http://localhost:8080/pdvs/partner/lng/{lng}/lat/{lat}

Get partners close to a point defined by latitude and longitude.

| Parameters | Value |
|        --- | ---   |
| lng | 34.1231313 |
| lat | -80.122313 |

## How does it work
The application runs a spring-boot with a H2 database (It is just for the example) with a table content:

| Field | type |
| ------ | ------ |
| id | INT4 |
| area_id | VARCHAR |
| trading_name | VARCHAR |
| owner_name | VARCHAR |
| document | VARCHAR |
| latitude | DOUBLE |
| longitude | DOUBLE |
| coverage_area_type | VARCHAR |
| coverage_area | VARCHAR |
| address_type | VARCHAR |
| address | VARCHAR |

The GeoJSon content is encode to be saved as a String. The coordinates are saved as String.

#### Area Id
The areaId is an identifier create to split the map. This help to get faster partners close to a point without to get all the list. The Id is created with the 5 firsts numbers of latitude and longitude,  with a 0 or 1 depending on if the value is negative or positive.

Example: Both point are going to search in the same area 

| lng | lat | result |
| ------ | ------ | ------ |
| 86,123214 | -32,342975 | “086123132342” |
| 86,123289 | -32,342456 | “086123132342” |

#### Point inside of the area

To identify a point inside of the area, I used "Winding number algorithm"

https://en.wikipedia.org/wiki/Point_in_polygon


## Test

I didn't have enough time to do the tests. I'm sorry :(